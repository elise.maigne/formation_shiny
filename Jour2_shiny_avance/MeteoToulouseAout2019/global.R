library(shiny)
library(ggplot2)
library(plotly)
library(leaflet)
library(htmltools)
library(leaflet)
library(shinythemes)

meteoAout2019 <- read.csv("data/meteoAout2019.csv")
meteoAout2019$Date <- as.POSIXct(meteoAout2019$Date)
meteoAout2019Station <- read.csv("data/meteoAout2019Station.csv")

rangeDate <- as.POSIXct(c(min(meteoAout2019$Date), max(meteoAout2019$Date)))
nomsStations <- sort(unique(meteoAout2019Station$station))

