# Exemples d'analyse sur l'ACP

# Récupération des jeux de données avec le titre du jeu de données
availData <- data.frame(data()$results)
listDataSets <- availData[availData[,"Item"] %in% c("USArrests", "swiss", "mtcars"),c("Item", "Title")]

print(listDataSets)

# Choix et chargement d'un dataset, ici le premier de la liste
currentDataset <- get(listDataSets[1,]$Item)

# ACP sur ce dataset
monACP <- stats::prcomp(currentDataset, center = TRUE, scale. = TRUE)

# Scores des individus
monACP$x

# Rotation des variables
monACP$rotation

# Exemples de graphiques
biplot(monACP, choices=c(1,2), col=c("blue", "red"), cex=c(0.8, 1))
screeplot(monACP)

# Equivalent en ggplot2
library(ggplot2)
dfScreeplot <- data.frame("Composante" = paste0("PC.", 1:length(monACP$sdev)),
                          "Variance" = monACP$sdev * monACP$sdev)

ggplot(data=dfScreeplot, aes(x=Composante, y=Variance)) + geom_bar(stat="identity")

