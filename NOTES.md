## A penser

Gestion des wd (pas de setwd() dans shiny !!)
Privilégier le mode projet Rstudio (une application = un projet). 
Bonnes pratiques = un dossier pour l'application
```
monapplication/
    monapplication.Rproj
    ui.R
    server.R
    data/
    www/
    R/ ou modules/
```

## Prérequis

### Packages

```
library(shiny)
library(DT)
library(rsconnect)
```


## A rajouter

```
isolate()
```
